import os
import sys
sys.path.append(".")
sys.path.append("..")

from dks import const
if os.path.exists(const.database):
    os.unlink(const.database)

from dks import model

model.Ingredient.createTable(ifNotExists=True)
model.Town.createTable(ifNotExists=True)

t = model.Town(name="Berlin")
tt = model.Town(name="Bremen")

i = model.Ingredient(name="Brimstone")
t.addIngredient(i)
tt.addIngredient(i)

ii = model.Ingredient(name="Choleric Base")
t.addIngredient(ii)

print("First town's ingredients", t.ingredients)
print("First ingredient's towns", i.towns)
print("Second ingredient's towns", ii.towns)

print(model.Town.byName('Bremen'))
