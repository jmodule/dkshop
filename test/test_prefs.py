#!/usr/bin/env python
#$Id: test_prefs.py,v 1.1 2005-10-21 15:16:59 jfriant Exp $

import sys
sys.path.append(".")
sys.path.append("..")

from dks import model

model.preferences.load("./dkshop.pref")

print("Load result:", model.preferences._status)

print(model.preferences.asStr())
