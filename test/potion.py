#!/usr/local/bin/env python

import sys
sys.path.append("..")
from dks.model import potion

test_strs = ["Essence o'Grace, Galen's               1",
             "Essence o'Grace, Michael S             1",
             "Essence o'Grace, Avicenna's (25q)      1",
             "Noxious Aroma, al-Razi's, (25q)        1",
             "Eyeburn, Solomon's"]


for s in test_strs:
    result = potion.re_formula.search(s)

    if result:
        print(result.group(1).strip())
        print(result.group(2))

print("----------------------------------------------------------------------")

potion.load('../potions.csv')
for n in potion.getAllNames():
    print(n)
    print("Ingredients: ", potion.getIngredients(n))
    print("Quality:", potion.getQuality(n))
    print("Tagged", potion.getTagged(n))

potion.save('test.csv')
