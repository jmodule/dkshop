#!/usr/bin/env python

from dks import const
from distutils.core import setup

setup(name="dkshop",
      version=const.version,
      author=const.author,
      author_email=const.email,
      url=const.url,
      packages=['dks', 'dks.model', 'dks.view'],
      scripts=['dkshop.py'],
      data_files=[('share/dkshop', ['potions.csv',
                                    'ingredients.csv',
                                    'towns.csv',
                                    'waterways.txt'])],
      requires=['urwid']
      )
