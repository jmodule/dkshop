"""Declares a shopping list object that stores ingredients purchased"""

import pickle


class ShoppingCart:
    """Holds the potions selected and the list of ingredients needed"""

    def __init__(self):
        self.shopping_list = {}
        self.potion_list = []
        self.ingredient_list = []

    def add(self, name, qty, ingredients):
        """Add the list of ingredents to the shopping list"""
        if ingredients is not None:
            for k in ingredients.keys():
                if k in self.shopping_list:
                    self.shopping_list[k] += ingredients[k] * qty
                else:
                    self.shopping_list[k] = ingredients[k] * qty

            self.potion_list.append((name, qty))


class ShoppingHistory:
    """Holds a list of 10 past shopping 'trips'"""
    carts = []

    def add(self, cart):
        """Add a cart to the list"""

        self.carts.insert(0, cart)

    def cnt(self):
        """Return the number of carts"""
        return len(self.carts)

    def save(self, filename):
        """Save the last 10 carts"""
        history = self.carts[:10]
        with open(filename, 'wb') as f:
            pickle.dump(history, f)

    def load(self, filename):
        """Load a list of shopping carts"""
        try:
            with open(filename, 'rb') as f:
                data = pickle.load(f)
            if isinstance(data, list):
                self.carts = data
            elif isinstance(data, ShoppingCart):
                self.carts = [data]
            else:
                raise Exception('Unknown data type for pickle: ' + repr(data))
        except (IOError, EOFError):
            self.carts = [ShoppingCart()]
