"""Potion database (term used loosly).

Potions are stored in a comma seperated file and loaded into the
dictionary. Access methods have been provided to serve as the
interface so I can change the back end as needed.

For reference the dict should look like so:

formula = { 'Name' : { 'ingredients' : { 'Item Name' : 2,
                                         'Aqua Regia' : 1 },
                                         'quality' : 25,
                                         'tagged' : True
                                       }

"""
import re

re_formula = re.compile("""((?:[A-Za-z,'\-]+\s)+)  # the potion name
                           \s+                     # seperated by spaces
                           (\d+)                   # and then the amount
                           """, flags=re.VERBOSE)

_all = dict()


class IngredientError(Exception):
    """Error when the quantity for an ingredient is wrong"""
    def __init__(self, name, line, invalid_qty):
        self.name = name
        self.line = line
        self.error = invalid_qty

    def __str__(self):
        msg = "Error with quantity for %s, on line %d: %s" % (self.name, self.line, self.error)
        return msg


class cFormula:
    """Holds the formula information"""

    def __init__(self, ingr=dict(), quality=0):
        """Set up the initial class"""
        self.ingredients = ingr
        self.quality = quality
        self.tagged = False

    def ingredients2string(self):
        """Return the ingredients as a string"""

        ingr = ["%s:%d" % (n, q) for (n, q) in self.ingredients.items()]
        s = ",".join(ingr)
        return s

    def __str__(self):
        """Return the class a string (for saving)"""

        s = "%s;%d" % (self.ingredients2string(), self.quality)
        return s


def getAllNames():
    """Return a sorted list of all the potion names"""
    return sorted(_all.keys())


def getAllTagged():
    """Return a list of only those formulas that have been tagged"""
    all = getAllNames()
    tag_list = list()
    for p in all:
        if getTagged(p):
            tag_list.append(p)
    if len(tag_list) == 0:
        tag_list = all
    return tag_list


def getIngredients(which):
    """Return a dict of the ingredients if the potion exists, otherwise None"""

    if which in _all:
        ingr = _all[which].ingredients
    else:
        ingr = None
    return ingr


def getQuality(which):
    """Return the quality as a string"""

    if which in _all:
        q = _all[which].quality
    else:
        q = None
    return q


def getTagged(which):
    """Return true if tagged, false if not"""
    if which in _all:
        t = _all[which].tagged
    else:
        t = False
    return t


def tag(which):
    """Toggle the tag on the specified formula and return a boolean"""
    success = False
    if which in _all:
        _all[which].tagged = (not _all[which].tagged)
        success = True
    return success


def save(filename):
    """Save the formulae to a file

    Ex: Thunderbolt, al-Tamini's;Anitmoni:1,Aqua Regia:2;25q;0"""

    with open(filename, 'w') as fout:
        for k in _all.keys():
            line = "%s;%s\n" % (k, str(_all[k]))
            fout.write(line)


def load(filename):
    """Load the formula data from a file

    file_list is a list of possible files to open

    If we don't find any files we'll die with an exception.

    """
    with open(filename, "r") as fin:
        cnt = 0
        for line in fin:
            cnt += 1
            parts = line.split(";")

            ingredients = dict()
            for item in parts[1].split(','):
                ingr = item.split(":")
                try:
                    qty = int(ingr[1])
                except ValueError:
                    raise IngredientError(ingr[0], cnt, ingr[1])
                ingredients[ingr[0]] = qty
            quality = int(parts[2])
            _all[parts[0]] = cFormula(ingredients, quality)
