"""A model for the list of ingredients found in each town"""

import sqlobject as db
from . import connect
db.sqlhub.processConnection = connect.connection


class Ingredient (db.SQLObject):
    name = db.StringCol(alternateID=True, length=20)
    price = db.IntCol(default=0)
    towns = db.RelatedJoin('Town',
                           joinColumn='ingredient',
                           otherColumn='town')

