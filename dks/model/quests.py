"""Holds quest records that are linked to towns"""

from datetime import datetime
import sqlobject as db
from . import connect
db.sqlhub.processConnection = connect.connection


class Quest(db.SQLObject):
    desc = db.StringCol(length=70)
    complete = db.BoolCol(default=False)
    town = db.ForeignKey('Town')
    timestamp = db.DateTimeCol(default=datetime.now)
