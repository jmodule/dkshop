"""Holds saint records that are linked to towns"""

import sqlobject as db
from . import connect
db.sqlhub.processConnection = connect.connection


class Saint(db.SQLObject):
    name = db.StringCol(length=70)
    known = db.BoolCol(default=False)
    town = db.ForeignKey('Town')
