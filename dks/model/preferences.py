"""Preferences file

Stores the user prefrences, espcecially the tagged potions

FILE FORMAT

The file uses the format, name = value, and there are a few recognized
names and they are all case insensitive.  But make note that the
values ARE case sensitive.

* tagged = <potion_name>

  Marks a potion name as tagged, multiple entries okay.

"""

import os

# this is just the names of the tagged formulae
_tagged = {}

_filename = ".dkshop"

_path = os.path.join(os.environ['HOME'], _filename)

_status = 0

def load(filename = _path):
    """Load the preferences from the file"""
    global _status

    try:
        with open(filename) as prefs:
            for line in prefs:
                parts = line.split('=')

                if parts[0].strip().lower() == 'tagged':
                    _tagged[parts[1].strip()] = True
    except IOError:
        _status = 1


def asStr():
    """Return the contents of the preferences model as a string"""
    s_out = ""

    for name in _tagged.keys():
        s_out += "tagged = %s\n" % name

    return s_out


def save():
    """Save the prefrences to a file"""
    global _status

    try:
        with open(_path, 'w') as prefs:
            prefs.write(asStr())
    except IOError:
        _status = 2


def toggleTag(name):
    """Change the tag status of the specified formula"""

    if name in _tagged:
        del _tagged[name]
    else:
        _tagged[name] = True

    return True


def isTagged(name):
    """Return true if the name is found, false if not"""
    return name in _tagged
