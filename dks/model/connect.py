"""This module is here to provide a standard connection point for the others

to use it you would write:

  import sqlobject as db
  import connect
  db.sqlhub.processConnection = connect.connection

"""
import os
import sqlobject
from dks import const

__db_filename = os.path.abspath(const.database)
__connection_string = 'sqlite:' + __db_filename

connection = sqlobject.connectionForURI(__connection_string)

