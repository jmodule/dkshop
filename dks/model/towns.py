"""A SQLObject model to store towns."""

import sqlobject as db
from . import connect
db.sqlhub.processConnection = connect.connection


class Town (db.SQLObject):
    name = db.StringCol(alternateID=True, length=25)
    swordsmith = db.IntCol(default=0)
    armorer = db.IntCol(default=0)
    fletcher = db.IntCol(default=0)
    physician = db.StringCol(length=10,default="")
    phstone = db.StringCol(length=10,default="")
    
    ingredients = db.RelatedJoin('Ingredient',
                                 joinColumn='town',
                                 otherColumn='ingredient')

    quests = db.MultipleJoin('Quest')

    saints = db.MultipleJoin('Saint')

    def tag(self):
        """Generate a tag based on the name"""

        tag_len = 5 # magic number here!!!
        parts = self.name.split()
        tag = parts[0][:tag_len - len(parts) + 1]
        for i in range(1, len(parts)):
            tag += parts[i][0:1]

        return tag.strip()
