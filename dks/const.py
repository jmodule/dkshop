"""Constants for the dkshop program

Right now mostly has paths to the data files.

"""

import os

_major = 2
_minor = 3
_build = 6

version = "%d.%d.%d" % (_major, _minor, _build)
author = "Jakim Friant"
email = "jakim@friant.org"
url = "https://jmodule.friant.org/spigot/"

M_title = "DKShop\nVersion %s\nWritten by %s <%s>" % (version, author, email)

# regular color scheme (blue bg, light gray text)
standard_palette = [
    ('background', 'light gray', 'dark blue'),
    ('plain', 'light gray', 'dark blue'),
    ('bright', 'white', 'dark blue'),
    ('button', 'dark blue', 'light gray'),
    ('editbx','yellow','dark blue'),
    ('editcp','light gray', 'dark blue'),
    ('editfc','white', 'black'),
    ('editbox', 'white', 'dark blue'),
    ('header', 'black', 'dark cyan'),
    ('title', 'light blue', 'black'),
    ('key','light cyan', 'dark blue'),
    # these are for the dialogs
    ('body','black','light gray', 'standout'),
    ('border','black','dark blue'),
    ('shadow','white','black'),
    ('selectable','black', 'dark cyan'),
    ('focus','white','dark blue','bold'),
    ('focustext','light gray','dark blue'),
    ]

#alternate color scheme
alternate_palette = [
    ('background', 'light gray', 'black'),
    ('plain', 'light gray', 'black'),
    ('bright', 'white', 'black', ('bold','standout')),
    ('button', 'dark blue', 'light gray', 'standout'),
    ('editbx','white','black'),
    ('editcp','yellow', 'black'),
    ('editfc','white', 'dark cyan', 'bold'),
    ('editbox', 'white', 'black'),
    ('focus', 'white', 'dark cyan', 'bold'),
    ('header', 'white', 'dark blue', 'standout'),
    ('title', 'light blue', 'black', 'bold'),
    ('key','light cyan', 'dark blue', 'underline'),
    ]

dlg_h = 22
dlg_w = 76

num_len = 3
name_len = 20
price_len = 4

mode = 'test'

path = {'live': os.path.join(os.environ['HOME'], ".local", "share", "dkshop"),
        'test': 'test'}

potion_file = os.path.join(path[mode], 'potions.csv')

last_list = os.path.join(path[mode], "last_list.pkl")

database = os.path.join(path[mode], "dkshop.db")

ingredient_file = os.path.join(path[mode], "ingredients.csv")

town_file = os.path.join(path[mode], "towns.csv")

map_file = os.path.join(path[mode], "waterways.txt")
