"""Class for a dialog listing the towns by top craftsmen"""

import urwid
from dks.controller import StandardDialog
from dks import model

# seperator = ('fixed', 2, urwid.AttrWrap(urwid.Text('|'), 'editcp'))


class Craft(StandardDialog):

    def __init__(self,  parent):
        """Initialize the screen elements

        The screen will be set up as 3 columns of two colums each,
        along with a standard header and footer:

            HHHHHHHHHHHHHHHHHHHH
            CCCCCCCCCCCCCCCCCCCC
            LLLLLL LLLLLL LLLLLL
            ccc cc ccc cc ccc cc
            FFFFFFFFFFFFFFFFFFFF

        Each column should have the town name and craft quality.
        
        """

        def buildColumn(column_name):
            """Return a list of columns with the town name and craft quality"""
            clist = [urwid.Text(column_name.title() + ":"),
                     urwid.Divider("-")]
            for t in model.Town.select(orderBy=column_name).reversed()[:10]:
                # I'm going to cheat here for now
                if column_name == "swordsmith":
                    quality = t.swordsmith
                elif column_name == "armorer":
                    quality = t.armorer
                elif column_name == "fletcher":
                    quality = t.fletcher
                else:
                    quality = ""
                w = urwid.Columns(
                    [urwid.Text(t.name),
                     ('fixed', 4, urwid.Text(str(quality), align='right'))],
                    2)
                clist.append(w)
            return clist

        self.parent = parent
        width = ("relative",  100)
        height = ("relative",  100)
        # create the header for later
        w = urwid.Text("Top 10 Craftsmen", align="center")
        header = urwid.AttrWrap(w, 'header')

        # and the footer
        w = urwid.Text(('button', "< Close >"), align='center')
        footer = urwid.AttrWrap(w, 'header')

        # then the body
        w = urwid.Columns([urwid.ListBox(buildColumn('swordsmith')),
                           urwid.ListBox(buildColumn('armorer')),
                           urwid.ListBox(buildColumn('fletcher'))],
                          3)
        w = urwid.Padding(w, ('fixed left', 4), ('fixed right', 4))
        w = urwid.Filler(w, ('fixed top', 2), ('fixed bottom', 2))
        w = urwid.AttrWrap(w, 'background')

        w = urwid.Frame(
            w,
            header=header,
            footer=footer,
            )

    # pad area around listbox
        w = urwid.Padding(w, ('fixed left',2), ('fixed right',2))
        w = urwid.Filler(w, ('fixed top',1), ('fixed bottom',1))
        w = urwid.AttrWrap(w, 'body')

        # "shadow" effect
        w = urwid.Columns( [w,('fixed', 2, urwid.AttrWrap(
                        urwid.Filler(urwid.Text(('border','  ')), "top")
                        ,'shadow'))])
        w = urwid.Frame( w, footer = 
                         urwid.AttrWrap(urwid.Text(('border','  ')),'shadow'))

        # outermost border area
        w = urwid.Padding(w, 'center', width )
        w = urwid.Filler(w, 'middle', height )
        w = urwid.AttrWrap( w, 'border' )
                
        self.top = urwid.Overlay(w, parent, 
                                      "center",  70, 
                                      "middle",  20)
