"""View module"""

__all__ = ['city', 'dialog', 'shop', 'craft', 'shopping_list', 'waterways']
