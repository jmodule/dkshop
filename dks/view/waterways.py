"""Class for a dialog that displays the waterways map file"""

import urwid
from dks.controller import StandardDialog


class Waterways(StandardDialog):

    def __init__(self, parent, filename):
        """Initialize the screen elements

        We're just display a text file and using the same method as
        the textbox dialog.
        
        """
        self.parent = parent
        l = []
        # read the whole file for the body
        for line in open(filename).readlines():
            l.append( urwid.Text( line.rstrip() ))
        body = urwid.ListBox(l)
        body = urwid.AttrWrap(body, 'selectable','background')
        body = urwid.Padding(body, ('fixed left', 1), ('fixed right', 1))
        # body = urwid.Filler(body, ('fixed top', 2), ('fixed bottom', 2))
        body = urwid.AttrWrap(body, 'background')


        # create the header for later
        header = urwid.Text("Waterways Map", align="center")
        header = urwid.AttrWrap(header, 'header')

        # and the footer
        footer = urwid.Text(('button', "< Close >"), align='center')
        footer = urwid.AttrWrap(footer, 'header')

        w = urwid.Frame(
            body,
            header=header,
            footer=footer,
            )
        self.top = urwid.Overlay(w, parent, 
                                      "center",  80, 
                                      "middle",  24)

