"""Class to display a shopping list for potion ingredients"""

import urwid

from dks.controller import StandardDialog
from dks import model, const


class ShoppingList(StandardDialog):

    def __init__(self, parent, cart = None,  town=None):
        """initialize the screen elements

        Arguments:
          potion_list -- a list of tuples with potion_name, qty
          ingredient_list -- another list of tuples with name, qty, towns.

        The dialog will show the potions at the top, then a list of
        columns with the ingredient, quantity needed, and towns that
        have them.  For example:

        ----------------------------------------------------------------------
        Making potions:
          Essence o'Grace, Michael S'  3
          Greatpower, Albertus M's     3
          Ironarm, Jabir's             3
        Need the following items:
          Alum                3   Bremen, Leipzig, Lubeck, Osnabruck, Ulm,
                                  Mainz, Koln, Duisberg
          Camomile            6   Berlin, Brandenburg, Erfurt, Frankfurt an
                                  der Oder, Leipzig, Lubeck, Osnabruck,
                                  Paderborn, Rostock, Ulm, Worms, Mainz, Koln
          Choleric Base       3
          Eastern Black Bean  6   Brandenburg, Frankfurt an der Oder,
                                  Osnabruck, Strassburg, Mainz, Duisberg
        ----------------------------------------------------------------------

        """

        def potionColumn(name, qty, cost):
            w = urwid.Columns(
                [('fixed', const.num_len, urwid.Text(str(qty), align='right')),
                 urwid.Text(name), 
                 ('fixed',  const.price_len,  urwid.Text(str(cost),  align='right'))],
                2)
            return w

        def ingredientColumn(name, qty,  curr_town):
            if "Base" not in name:
                where = [t.name for t in model.Ingredient.byName(name).towns]
                where.sort()
            elif curr_town is not None and name in [i.name for i in curr_town.ingredients]:
                where = [curr_town.name]
            else:
                where = []
            w = urwid.Columns(
                [('fixed', const.num_len, urwid.Text(str(qty), align='right')),
                 ('fixed', const.name_len, urwid.Text(name)),
                 urwid.Text(", ".join(where))],
                2)
            return w
            
        def getPotionCost(which):
            """Return the cost for purchasing all the ingredients for this potion"""
            cost = 0
            ingredients = model.potion.getIngredients(which)
            if ingredients is not None:
                for name in ingredients.keys():
                    ingr = model.Ingredient.byName(name)
                    cost += ingr.price * ingredients[name]
            else:
                cost = -1
            return cost
        
        self.parent = parent
        history = model.ShoppingHistory()
        history.load(const.last_list)
        model.potion.load(const.potion_file)
        
        blank = urwid.Text("")

        # create the header
        w = urwid.Text("Shopping List (%d carts)" % history.cnt(), align="center")
        self.header = urwid.AttrWrap(w, 'header')

        # and footer
        w = urwid.Text(('button', "< close >"), align="center")
        self.footer = urwid.AttrWrap(w, 'header')

        items = []

        for cart in history.carts:
            # then start on the body
            items.extend([
                urwid.Columns([('fixed', const.num_len, urwid.Text('Qty')),
                               urwid.Text('Potions'),
                               ('fixed',  const.price_len,  urwid.Text('Cost'))], 2),
                urwid.Columns([('fixed', const.num_len, urwid.Divider("-")),
                               urwid.Divider("-"), 
                               ('fixed',  const.price_len,  urwid.Divider("-"))], 2)
                ])
            for name,  qty in cart.potion_list:
                cost = getPotionCost(name)
                items.append(potionColumn(name,  qty,  cost))

            items.extend([
                urwid.Text(''),
                urwid.Columns([('fixed', const.num_len, urwid.Text('Qty')),
                               ('fixed', const.name_len, urwid.Text('Name')),
                               urwid.Text('Where Found')], 2),
                urwid.Columns([('fixed', const.num_len, urwid.Divider("-")),
                               ('fixed', const.name_len, urwid.Divider("-")),
                               urwid.Divider("-")], 2),
                ])
            for k in sorted(cart.shopping_list.keys()):
                items.append(ingredientColumn(k, cart.shopping_list[k], town))
                
            items.append(urwid.Divider('*', 1, 1))

        w = urwid.ListBox(items)
        w = urwid.Padding(w, ('fixed left', 2), ('fixed right', 2))
        w = urwid.AttrWrap(w, 'background')
        w = urwid.Frame(w, self.header, footer=self.footer)
        self.top = urwid.Overlay(w, parent, 
                                              "center",  80, #width
                                              "middle",  24) #height
