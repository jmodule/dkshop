import urwid.curses_display
import urwid

import os

from dks import const
from dks import model
from dks.controller import StandardScreen

from dks.view import dialog
from dks.view.shopping_list import ShoppingList

# note this creates an object that is referenced so don't try to do a
# set_text() unless you plan on changing every place it's used!
blank = urwid.Text("")

M_help = """

F1 -- This help screen
F2 -- Generate a shopping list
F3 -- Redisplay the last shopping list
F4 -- Show the quality of a potion
F6 -- Tag a potion
F7 -- Toggle the filter to display only tagged potions

ESC, CTRL+Q -- Exit the screen"""


class Shop(StandardScreen):

    def __init__(self, parent=None,  town=None):
        """set up some important lists"""
        self.parent = parent
        self.curr_town = town
        self.header_text = ('header',
                            [ 'DK Shopping List     ',
                              ('key', 'F1'), ' about  ',
                              ('key', 'F2'), ' shop  ',
                              ('key', 'F3'), ' show  ',
                              ('key', 'F6'), ' tag  ',
                              ('key', 'F7'), ' filter  ',
                              ]
                            )
        self.commands = {'f1': self.about,
                         'f2': self.shop,
                         'f3': self.show,
                         'f4': self.quality,
                         'f6': self.tag,
                         'f7': self.filter,
                         }

        # Create the header
        #
        self.header = urwid.AttrWrap(urwid.Text(self.header_text), 'header')

        # Create the footer
        #
        self.status = urwid.Text("Choose your potion from the list above.",
                                 align="center")
        self.footer = urwid.AttrWrap(self.status, 'header')

        self.filter_on = False

        # load the preferences
        model.preferences.load()

        # Initialize the potions and load the list box
        model.potion.load(const.potion_file)
        self.initPotions()
        if self.parent is not None:
            self.top = urwid.Overlay(self.top, parent,
                                     "center",  80, #width
                                     "middle",  24) #height

    def run(self):
        """A custom main loop to handle edit boxes getting focus

        The potion quality is displayed in the status bar whenever
        the user moves up or down.

        """
        size = self.ui.get_cols_rows()

        while True:
            self.draw_screen(size)
            keys = self.ui.get_input()
            if "esc" in keys or "ctrl q" in keys:
                break
            for k in keys:
                if k == "window resize":
                    size = self.ui.get_cols_rows()
                    continue
                if k in self.commands:
                    self.commands[k]()
                else:
                    if self.top.selectable():
                        self.top.keypress(size, k)
                # We don't want to check the focus until all movement is done
                if k in ('up', 'down', 'page up', 'page down'):
                    self.quality()
        self.on_exit()

    def addPotion(self, name):
        """Add a potion line to the list"""

        if model.preferences.isTagged(name):
            style = 'focus'
        else:
            style = 'plain'

        caption = "%-40s" % name
        value = 0

        self.qty.append(urwid.IntEdit((style, caption), value))

        w = urwid.AttrWrap(self.qty[len(self.qty)-1], 'editbx', 'editfc')
        self.potions.append(w)

    def initPotions(self):
        """Load the list of potions in to the list box"""
        
        potion_list = model.potion.getAllNames()
        
        self.potions = list()
        self.qty = list()

        for p in potion_list:
            if not self.filter_on or model.preferences.isTagged(p):
                self.addPotion(p)
    
        self.listbox = urwid.ListBox(self.potions)

        # Create the main frame
        #
        w = urwid.Padding(self.listbox, ('fixed left', 2), ('fixed right', 35))
        w = urwid.AttrWrap(w, 'editcp')
        self.top = urwid.Frame(w, self.header, footer=self.footer)

    def on_exit(self):
        """clean up"""
        model.potion.save(const.potion_file)
        model.preferences.save()

    def shop(self):
        """Generate the shopping list and pass to the display screen"""
        history = model.ShoppingHistory()
        history.load(const.last_list)
        
        cart = model.ShoppingCart()
        for q in self.qty:
            result = model.potion.re_formula.search(q.get_text()[0])
            if result:
                name = result.group(1).strip()
                ingredients = model.potion.getIngredients(name)
                try:
                    qty = int(result.group(2))
                except ValueError:
                    qty = 0
                if qty > 0:
                    cart.add(name, qty, ingredients)
        
        # Attempt to save the list we just generated
        try:
            history.add(cart)
            history.save(const.last_list)
            self.status.set_text("Saved list to " + const.last_list)
        except IOError:
            self.status.set_text("Error saving list to %s" % const.last_list)

        self.show()

    def show(self):
        """Redisplay the generated list"""
        ShoppingList(self.top, town=self.curr_town).main(self.ui)

    def tag(self):
        """Tag a potion to filter later"""
        widget, pos = self.listbox.get_focus()
        result = model.potion.re_formula.search(widget.get_text()[0])
        if result is not None:
            which = result.group(1).strip()
            if model.preferences.toggleTag(which):
                msg = "Set Tag"
            else:
                msg = "Failed Tag"
        else:
            which = None
            msg = "Failed Tag"
        
        self.status.set_text("%s on %s to %s" % (msg, which, model.preferences.isTagged(which)))
        self.initPotions()
        self.listbox.set_focus(pos, 'above')
        
    def filter(self):
        """Toggle the filter of tagged potions on and off"""
        if not self.filter_on:
            self.filter_on = True
        else:
            self.filter_on = False
            
        self.initPotions()

    def about(self):
        """Display an about/help screen"""
        d = dialog.do_msgbox(self.top, const.M_title + M_help, 22, 0)
        d.main(self.ui)

    def quality(self):
        """Display the quality of the potion"""
        widget, pos = self.listbox.get_focus()
        if widget.selectable():
            # WARNING, I'm only expecting an Edit box, which is fine
            # for now, but be careful of adding widgets to the list
            # box in the future
            result = model.potion.re_formula.search(widget.get_text()[0])
            if result is not None:
                which = result.group(1).strip()
                qual = model.potion.getQuality(which)
                self.status.set_text("Potion quality: %d" % qual)
