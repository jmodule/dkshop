import os
import sys
import types
import time

import urwid
import urwid.curses_display

from dks.view import dialog
from dks.view.shop import Shop
from dks.view.craft import Craft
from dks.view.shopping_list import ShoppingList
from dks.view.price_list import PriceList
from dks.view.waterways import Waterways

from dks import model, const
from dks.controller import StandardScreen

E_select_town = "Select a city before entering details"
E_bad_town = "Invalid town name"
E_report_canceled = "No report selected"
M_saved_town = "Saved details of %s to the database"
M_welcome = "Welcome the the Darklands Shopper and Town Database"
M_help = """

F1  - Display this help screen
F2  - Select a city to display/edit
F4  - Edit the alchemy materials list
F6  - Save the city to the database
F7  - Switch to the the formula shopper
F8  - Switch to the alchemical price list
F9  - Display the reports menu
ESC, CTRL+Q - Exit the program"""

blank = urwid.Text("")


class City(StandardScreen):
    header_text = ('header',
                   [ 'City Info     ',
                     ('key', 'F1'), ' help  ',
                     ('key', 'F2'), ' select  ',
                     ('key', 'F4'), ' alch  ',
                     ('key', 'F6'), ' save  ',
                     ('key', 'F7'), ' shop  ',
                     ('key', 'F9'), ' reports  ',
                     ]
                   )
    curr_town = None

    def __init__(self):
        """Initialize the screen elements"""

        self.commands = {'f1': self.help,
                         'f2': self.select,
                         'f4': self.alch,
                         'f6': self.save,
                         'f7': self.shop,
                         'f8': self.prices, 
                         'f9': self.runReport,
                         'ctrl d': self.deleteQuest, 
                         }

        self.reports = [
            'CRFT', 'Craftsmen',
            'QEST', 'Quests',
            'SNTS', 'Saints',
            'SLST', 'Shopping Lists',
            'WMAP', 'Waterways Map',
            ]
        
        self.report_func = { 'WMAP' : self.displayMap,
                             'QEST' : self.allQuests,
                             'SLST' : self.shoppingList,
                             'CRFT' : self.topCraftsmen,
                             'SNTS' : self.saints,
                             }

        # create the header
        self.header = urwid.AttrWrap(urwid.Text(self.header_text), 'header')

        # and footer
        self.status = urwid.Text(M_welcome)
        self.footer = urwid.AttrWrap(self.status, 'header')
        
        # now we'll begin creating the screen objects that I want to
        # reference later by assigning them to class members and the
        # rest just get added to the contents list
        #
        self.contents = []

        self.city_name = urwid.Text("")

        self.saints = self.EditList(4)
        self.formula = self.EditList(4)

        self.swordsmith = urwid.IntEdit(('editcp', "Swordsmith: "), "")
        self.armorer = urwid.IntEdit(('editcp', "Armorer: "), "")
        self.fletcher = urwid.IntEdit(('editcp', "Fletcher: "), "")

        self.physician = urwid.Edit(('editcp', "Physician's Skill: "), "")
        self.phstone = urwid.Edit(('editcp', "Philosopher's Stone: "), "")

        self.alch_materials = urwid.Text("")

        self.quests = self.EditList(4)
        self.quests_completed = [urwid.CheckBox('Done') for i in range(4)]
        quest_list = []
        for i in range(len(self.quests)):
            quest_list.append(
                urwid.Columns([urwid.AttrWrap(self.quests[i],
                                              'editbx',
                                              'editfc'),
                               ('fixed', 10, self.quests_completed[i])],
                              1)
                )
        self.quest_group = urwid.Pile(quest_list)
                
        self.contents.extend(
            [urwid.Columns([('fixed', 6, urwid.Text("Name:")),
                            urwid.AttrWrap(self.city_name,'editbx'),
                            blank]),
             blank,
             urwid.GridFlow([urwid.Text("Saints:"),
                             urwid.Text("Formulae:")
                             ],
                            38, 2, 0, 'left'),
             urwid.GridFlow([urwid.AttrWrap(self.saints[0],'editbx','editfc'),
                             urwid.AttrWrap(self.saints[2],'editbx','editfc'),
                             urwid.AttrWrap(self.formula[0],'editbx','editfc'),
                             urwid.AttrWrap(self.formula[2],'editbx','editfc'),
                             urwid.AttrWrap(self.saints[1],'editbx','editfc'),
                             urwid.AttrWrap(self.saints[3],'editbx','editfc'),
                             urwid.AttrWrap(self.formula[1],'editbx','editfc'),
                             urwid.AttrWrap(self.formula[3],'editbx','editfc'),
                             ],
                            19, 1, 0, 'left'),
             blank,
             urwid.GridFlow([urwid.Text('Equipment Quality:'),
                             urwid.Text('Miscellaneous:')],
                            38, 2, 0, 'left'),
             urwid.GridFlow([urwid.AttrWrap(self.swordsmith,
                                            'editbx',
                                            'editfc'),
                             urwid.AttrWrap(self.physician,
                                            'editbx',
                                            'editfc'),
                             urwid.AttrWrap(self.armorer,
                                            'editbx',
                                            'editfc'),
                             urwid.AttrWrap(self.phstone,
                                            'editbx',
                                            'editfc'),
                             urwid.AttrWrap(self.fletcher,
                                            'editbx',
                                            'editfc'),
                             blank],
                            35, 10, 0, 'left'),
             blank,
             urwid.Text('Alchemical Materials: '),
             urwid.AttrWrap(self.alch_materials, 'editbx'),
             blank,
             urwid.Text('Quests:'),
             self.quest_group,
             blank,
             ]
            )

        self.body = urwid.ListBox(self.contents)
        w = urwid.AttrWrap(self.body, 'background')
        self.top = urwid.Frame(w, self.header, footer=self.footer)

    def help(self):
        """Display a help dialog"""

        if self.curr_town:
            self.save()
        d = dialog.do_msgbox(self.top, const.M_title + M_help, 22, 0)
        d.main(self.ui)

    def updateScreen(self):
        """Update the contents from the current town record"""
        if self.curr_town is not None:
            
            self.city_name.set_text(self.curr_town.name)
            self.swordsmith.set_edit_text(str(self.curr_town.swordsmith))
            self.armorer.set_edit_text(str(self.curr_town.armorer))
            self.fletcher.set_edit_text(str(self.curr_town.fletcher))
            self.physician.set_edit_text(self.curr_town.physician)
            self.phstone.set_edit_text(self.curr_town.phstone)
            
            ingr = [i.name for i in self.curr_town.ingredients]
            self.alch_materials.set_text(", ".join(ingr))

            cnt = 0
            for saint in self.curr_town.saints:
                self.saints[cnt].set_edit_text(saint.name)
                cnt += 1
                if cnt >= len(self.saints):
                    break
            # clear out any remaining edit boxes
            while cnt < len(self.saints):
                self.saints[cnt].set_edit_text("")
                cnt += 1

            cnt = 0
            for quest in self.curr_town.quests:
                self.quests[cnt].set_edit_text(quest.desc)
                self.quests_completed[cnt].set_state(quest.complete)
                cnt += 1
                if cnt >= len(self.quests):
                    break
            # clear out any remaining edit boxes
            while cnt < len(self.quests):
                self.quests[cnt].set_edit_text("")
                self.quests_completed[cnt].set_state(False)
                cnt += 1

    def save(self):
        """Write the update town info to the db"""
        if self.curr_town is not None:
            self.curr_town.set(
                swordsmith = self.swordsmith.value(),
                armorer = self.armorer.value(),
                fletcher = self.fletcher.value(),
                physician = self.physician.get_edit_text(),
                phstone = self.phstone.get_edit_text()
                )

            # To save the quests, I need to first update the
            # existing quests and then add the new ones or delete ones
            # that are done.
            saved_quests = self.curr_town.quests
            cnt = 0
            while cnt < len(saved_quests):
                # *FIXME* delete empty quests
                saved_quests[cnt].desc = self.quests[cnt].get_edit_text()
                saved_quests[cnt].complete = self.quests_completed[cnt].get_state()
                cnt += 1

            while cnt < len(self.quests):
                if self.quests[cnt].get_edit_text().strip() != "":
                    new_quest = model.Quest(
                        desc=self.quests[cnt].get_edit_text(),
                        town=self.curr_town,
                        complete=self.quests_completed[cnt].get_state(),
                        )
                cnt += 1

            # same for saints
            saved_saints = self.curr_town.saints
            cnt = 0
            while cnt < len(saved_saints):
                saved_saints[cnt].desc = self.saints[cnt].get_edit_text()
                cnt += 1

            while cnt < len(self.saints):
                if self.saints[cnt].get_edit_text().strip() != "":
                    new_saint = model.Saint(
                        name=self.saints[cnt].get_edit_text(),
                        town=self.curr_town)
                cnt += 1
            
            self.status.set_text(M_saved_town % self.curr_town.name)
        else:
            self.status.set_text(E_select_town)

    def on_exit(self):
        """Make sure any changes are saved before exiting"""
        if self.curr_town:
            self.save()

    def EditList(self, cnt):
        l = [urwid.Edit(('editcp', "%d. " % (i + 1)), "") for i in range(cnt)]
        return l

    def select(self):
        """Select and set the current town record"""
        if self.curr_town:
            self.save()
            selected = self.curr_town.name
        else:
            selected = ""
            
        tag_xref = dict()
        town_list = list()
        for t in model.Town.select(orderBy='name'):
            tag_xref[t.tag()] = t.name
            if t.name == selected:
                flag = True
            else:
                flag = False
            town_list.extend([t.tag(), t.name, flag])

        d = dialog.do_radiolist(self.top, "Select a town", 0, 0, 10, * town_list)
        exitcode, exitstring = d.main(self.ui)
        if exitcode == 0 and exitstring in tag_xref:
            self.curr_town = model.Town.byName(tag_xref[exitstring])
            self.updateScreen()

    def shop(self):
        """Display the Alchemy Formula Shopping screen"""
        if self.curr_town:
            self.save()
        Shop(self.top,  town=self.curr_town).main(self.ui)

    def prices(self):
        """Display the price list dialog"""
        if self.curr_town:
            self.save()
        PriceList(self.top).main(self.ui)
    
    def alch(self):
        """Display a list to select alchemical materials from"""

        if self.curr_town is not None:
            self.save()
            town_ingr = [i.name for i in self.curr_town.ingredients]
        else:
            town_ingr = []

        ingr_list = []
        tag_xref = dict()
        for t in model.Ingredient.select(orderBy='name'):
            tag = t.name[:5].strip()
            tag_xref[tag] = t.name
            if t.name in town_ingr:
                flag = True
            else:
                flag = False
            ingr_list.extend([tag, t.name, flag])

        d = dialog.do_checklist(self.top, 
                                "Select alchemical materials",
                                0,
                                0,
                                10,
                                * ingr_list)
        exitcode, exitstring = d.main(self.ui)

        if exitcode == 0:
            for t in exitstring.replace('"', '').split():
                if t in tag_xref:
                    # add the ingredients to the town
                    if self.curr_town is not None:
                        # don't add it if it's already in the list
                        if tag_xref[t] not in town_ingr:
                            ingr = model.Ingredient.byName(tag_xref[t])
                            self.curr_town.addIngredient(ingr)
            self.updateScreen()

    def allQuests(self):
        """Display a dialog showing all the outstanding quests"""
        if self.curr_town:
            self.save()
        quest_list = []
        for q in model.Quest.select(model.Quest.q.complete == False,
                                    orderBy=model.Quest.q.timestamp):
            if q.desc.strip() != "":
                quest_list.extend([q.town.name, q.desc])

        d = dialog.do_menu(self.top,
                                "Outstanding Quests",
                                const.dlg_h,
                                const.dlg_w,
                                10,
                                * quest_list)
        exitcode, exitstring = d.main(self.ui)
        if exitcode == 0:
            try:
                new_town = model.Town.byName(exitstring)
                self.curr_town = new_town
                self.updateScreen()
            except:
                self.status.set_text(E_bad_town)

    def shoppingList(self):
        """Display the last shopping list"""
        if self.curr_town:
            self.save()
        ShoppingList(self.top,  town=self.curr_town).main(self.ui)

    def topCraftsmen(self):
        """Display a dialog with a report of the top armorers, smiths, etc"""
        if self.curr_town:
            self.save()
        Craft(self.top).main(self.ui)

    def saints(self):
        """Display the list of entered saints and matching towns"""
        if self.curr_town:
            self.save()
        saint_list = []
        for s in model.Saint.select(orderBy=model.Saint.q.name):
            saint_list.extend([s.name, s.town.name])

        d = dialog.do_menu(self.top, "Saints", const.dlg_h, const.dlg_w, 10, *saint_list)
        d.main(self.ui)

    def displayMap(self):
        """Display a dialog with the waterways map"""
        if self.curr_town is not None:
            self.save()
        d = Waterways(self.top, const.map_file)
        d.main(self.ui)

    def deleteQuest(self):
        """Delete the currently selected quest.
        
        Note that there doesn't seem to be a simple way to determine if the
        quest pile has focus, so pressing ctrl-d when the pile doesn't have
        focus will always return the first Edit widget.
        
        """
        #top_widget = self.body.get_focus()
        wcol = self.quest_group.get_focus()
        if isinstance(wcol,  urwid.widget.Columns):
            w = wcol.get_focus()
            if isinstance(w,  urwid.widget.AttrWrap):
                wtxt = w.get_w()
                (qtxt, attr) = wtxt.get_text()
                # make sure there is actually a quest description before
                # prompting the user
                if wtxt.get_edit_text().strip() != "":
                    msg = "Delete Quest: \n" + qtxt
                    dlg_h = 8
                    dlg_w = len(qtxt) + 6
                    d = dialog.do_yesno(self.top, msg, dlg_h, dlg_w)
                    exitcode, exitstring = d.main(self.ui)
                    if exitcode == dialog.EXIT_OKAY:
                        try:
                            which = int(qtxt[:1]) - 1
                            q = self.curr_town.quests[which]
                            model.Quest.delete(q.id)
                            self.updateScreen()
                        except Exception as e:
                            self.msgbox("Error deleting %s\n%s" % (qtxt, e))
        
    def purgeQuests(self):
        """Delete completed quests for the current town"""
        pass

    def runReport(self):
        """Display a menu with all the reports"""

        d = dialog.do_menu(self.top,
                           "Reports",
                           const.dlg_h,
                           const.dlg_w,
                           10,
                           *self.reports)
        result = d.main(self.ui)
        if result[1] in self.report_func:
            f = self.report_func[result[1]]
            f()
        else:
            # update the status bar with a message
            self.status.set_text(E_report_canceled)

    def msgbox(self, msg):
        """Display a standard dialog box with a close button only"""
        dialog.do_msgbox(self.top, msg, 0, 0).main(self.ui)
