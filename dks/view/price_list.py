"""Alchemical Component Price List

This dialog let's the user edit the prices of the ingredients, which adjust
during the game according to the Speak Common skill of the party.

"""
import urwid.curses_display
import urwid

from dks import model
from dks.controller import StandardScreen


class PriceList(StandardScreen):
    """Dialog Window that displays the price list and accumulates the input to
    be saved to the database.
    """
    def __init__(self, parent=None):
        self.parent = parent
        self.header_text = ('header', ['DK Price List    '])
        
        # create the header
        self.header = urwid.AttrWrap(urwid.Text(self.header_text),  'header')
        
        # create the footer
        self.status = urwid.Text("Enter the alchemical compnent prices.",  align='center')
        self.footer = urwid.AttrWrap(self.status,  'header')
        
        self.ingredients = model.Ingredient.select(orderBy='name')
        self.prices = []
        self.items = []
        for ingr in self.ingredients:
            caption = "%-40s" % ingr.name
            try:
                value = int(ingr.price)
            except ValueError:
                value = 0
            self.prices.append(urwid.IntEdit(('plain',  caption),  value))
            self.items.append(urwid.AttrWrap(self.prices[len(self.prices) - 1],  'editbx',  'editfc'))
        
        self.listbox = urwid.ListBox(self.items)
        
        # create the main frame
        w = urwid.Padding(self.listbox,  ('fixed left',  2),  ('fixed right',  35))
        w = urwid.AttrWrap(w,  'editcp')
        w = urwid.Frame(w,  self.header,  footer=self.footer)
        
        if self.parent is not None:
            self.top = urwid.Overlay(w,  parent, 
                                     'center',  80, # width 
                                     'middle',  24) # height
        else:
            self.top = w

    def on_exit(self):
        """Save the data as we exit"""
        for i in range(len(self.prices)):
            self.ingredients[i].price = self.prices[i].value()
