"""These classes handle the standard commands and have the main loop

Both should be used as parent classes and be inherited from by the
classes in view.  For example:

class City(StandardScreen)
class Crafts(StandardDialog)

"""
import urwid
import urwid.curses_display

from dks import const


class StandardScreen:
    """A prototype screen with the main loop and some basic initialization"""

    top = None                       # the main window widget
    palette = const.standard_palette # standard colors
    commands = {}                    # a dictionary mapping keys to commands
    parent = None                      # a possible parent window/screen
        
    def main(self, ui = None):
        if ui is None:
            self.ui = urwid.curses_display.Screen()
        else:
            self.ui = ui
        if self.parent is None:
            self.ui.register_palette( self.palette )
            return self.ui.run_wrapper( self.run )
        else:
            return self.run()

    def draw_screen(self, size):
        canvas = self.top.render(size, focus=True)
        self.ui.draw_screen(size, canvas)

    def run(self):
        size = self.ui.get_cols_rows()

        while True:
            self.draw_screen(size)
            keys = self.ui.get_input()
            if "esc" in keys or "ctrl q" in keys:
                break
            for k in keys:
                if k == "window resize":
                    size = self.ui.get_cols_rows()
                    continue
                if k in self.commands:
                    self.commands[k]()
                else:
                    if self.top.selectable():
                        self.top.keypress(size, k)
        self.on_exit()

    def on_exit(self):
        """Blank function for things that need to be done before exiting"""
        pass


class StandardDialog (StandardScreen):
    """A prototype dialog class with slightly different default keys"""

    def run(self):
        """This is the main loop of the dialog"""

        size = self.ui.get_cols_rows()
        okay = False
        while not okay:
            self.draw_screen(size)
            keys = self.ui.get_input()
            for k in keys:
                k = k.lower()
                if k == 'window resize':
                    size = self.ui.get_cols_rows()
                    continue
                if k == 'enter' or k == 'esc':
                    okay = True
                    continue
                self.top.keypress(size, k)
