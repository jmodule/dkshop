#!/usr/bin/env python

import os
import sys

from dks import const, model


def main():
    """Create a default SQLite database

    It is populated with towns and alchemical ingredients from two flat files:
      ingredients.csv
      towns.csv
    """

    try:
        ingr_file = open(const.ingredient_file)
    except IOError:
        print("Error opening", const.ingredient_file)
        sys.exit(1)
    try:
        town_file = open(const.town_file)
    except IOError:
        print("Error opening", const.town_file)
        sys.exit(1)
        
    # now create and load the database
    if os.path.exists(const.database):
        os.unlink(const.database)

    model.Ingredient.createTable()
    model.Town.createTable()
    model.Quest.createTable()
    model.Saint.createTable()

    print("Building town table...")
    for line in town_file:
        t = model.Town(name=line.strip())
        print("Created record for", t.name)
    town_file.close()

    print("Building ingredient table...")
    for line in ingr_file:
        i = model.Ingredient(name=line.strip())
        print("Created record for", i.name)
    town_file.close()


if __name__ == "__main__":
    main()
