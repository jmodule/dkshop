"""upgradedb.py converts the database schaema to support ingredient prices.

It has to be done in two steps.  First to copy the RelatedJoin data into a list
and pickle it for later, and then drop the existing table.  Then we can
recreate the table with the new schaema.

"""

import pickle
import os
from optparse import OptionParser
import sys
from dks import model

UPGRADE_FN = os.path.join(os.environ['HOME'], "upgrade.pkl")
USAGE = "usage: %prog [options] TABLE_NAME"

function_table = dict()


def setUpgradeData(saved):
    """Pickle the table data and save it to a file for later."""
    fd = open(UPGRADE_FN,  'w')
    pickle.dump(saved,  fd)
    fd.close()


def getUpgradeData():
    """Load the pickled data from the file (if it exists)"""
    if os.access(UPGRADE_FN,  os.F_OK):
        fd = open(UPGRADE_FN,  'r')
        saved = pickle.load(fd)
        fd.close()
    else:
        raise IOError("No restore file, You must run step one first.")
    return saved


def IngredientSave():
    """Run this before you install the new code (via make install)"""
    saved = dict()
    all_ingr = model.Ingredient.select()
    for ingr in all_ingr:
        saved[ingr.name] = [t.id for t in ingr.towns]
    setUpgradeData(saved)
    model.Ingredient.dropTable()
    print("Now install the new code.")


def IngredientUpgrade():
    """Run this after you install the new code"""
    model.Ingredient.createTable()
    saved = getUpgradeData()
    for name in saved.keys():
        ingr = model.Ingredient(name=name)
        for tid in saved[name]:
            town = model.Town.get(tid)
            ingr.addTown(town)


function_table['Ingredient'] = (IngredientSave, IngredientUpgrade)


def QuestSave():
    """Dump the quest table to a temporary file"""
    saved = list()
    for quest in model.Quest.select():
        saved.append({'desc': quest.desc,
                      'complete': quest.complete,
                      'town': quest.town.id})
    setUpgradeData(saved)
    model.Quest.dropTable()


def QuestUpgrade():
    """Load the new quest table and populate it from the saved data"""
    model.Quest.createTable()
    saved = getUpgradeData()
    for item in saved:
        town = model.Town.get(item['town'])
        quest = model.Quest(desc=item['desc'],
                            complete=item['complete'],
                            town=town)


function_table['Quest'] = (QuestSave, QuestUpgrade)


def main():
    parser = OptionParser(usage=USAGE)
    parser.add_option("-u", "--upgrade", dest="upgrade",
                      action="store_true", default=False,
                      help="Run the table upgrade (second step)")
    parser.add_option("-l", "--list", dest="show_list",
                      action="store_true", default=False,
                      help="Show tables that can be upgraded")
    (options, args) = parser.parse_args()

    if options.show_list:
        print("Tables that can be upgraded:")
        for name in function_table.keys():
            print("\t-", name)
        sys.exit(0)

    if len(args) < 1:
        parser.error("You must specify a TABLE_NAME")
    else:
        if args[0] in function_table:
            if options.upgrade:
                try:
                    function_table[args[0]][1]()
                    # delete the pickle file if the upgrade worked
                    os.unlink(UPGRADE_FN)
                except IOError as e:
                    print("Error:", e)
                    sys.exit(1)
            else:
                function_table[args[0]][0]()
        else:
            parser.error("%s is not a valid table" % args[0])


if __name__ == '__main__':
    main()
