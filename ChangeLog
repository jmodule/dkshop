2006-02-01  Jakim Friant  <jfriant@glec.cfcc.edu>

	* dks/view/city.py (City.allQuests): Added the option to select
	the town when viewing the outstanding quests.

2006-01-20  Jakim Friant  <jfriant@glec.cfcc.edu>

	* dks/model/shopping_cart.py (ShoppingHistory.cnt): Added a class
	to manage a list of past carts.

	* dks/view/shopping_list.py (ShoppingList.__init__): Modified the
	shopping list to accept a history of carts seperated by a divider.

2006-01-19  Jakim Friant  <jfriant@glec.cfcc.edu>

	* dks/view/shopping_list.py
	(ShoppingList.__init__.ingredientColumn): Finally figured out I
	could simply sort the list of towns where the ingredient is found.

	* dks/view/city.py (City.on_exit): Making sure the city is saved
	when exiting.

2006-01-19  Jakim Friant  <jfriant@cfcc.edu>

	* dks/view/city.py (City.runReport): Moving all the reports to one
	menu so I don't have to remember all the key shortcuts.

2005-11-22  Jakim Friant  <jfriant@glec.cfcc.edu>

	* dks/view/shop.py (Shop.run): Fixed the problem of the wrong
	quality begin displayed by moving the function down past the point
	where the movement keys are processed.

2005-11-21  Jakim Friant  <jfriant@glec.cfcc.edu>

	* dks/view/shop.py (Shop.detail): Added a function to display the
	potion quality in the status line.  Still need to make it
	automatic when a line gets focus.
	(Shop.run): Created a custom run routine (main loop) so I could
	call the quality function as the focus moves up or down.

2005-11-15  Jakim Friant  <jfriant@glec.cfcc.edu>

	* dks/const.py (standard_palette): Working on changing the
	standard palette to a black background with yellow and white text
	-- a scheme inspired by a bookstore POS interface.  I'd like a way
	to switch between the old blue scheme and the new one.  After
	working with it for a while though, I've moved back to an altered
	blue scheme until I can work something out for the dialog.

	* dks/view/dialog.py (DialogDisplay.run): Added the Escape key to
	trigger a cancel event (sort of).

	* dks/view/waterways.py (Waterways): Added a new dialog to show
	the waterways map.

	* dks/view/city.py (City.shop): Displaying another dialog/screen
	erases any text box entry, so I added a save statment to all the
	functions that create a dialog.

2005-11-11  Jakim Friant  <jfriant@glec.cfcc.edu>

	* dks/view/city.py (City.saints): Added a report of known saints
	and the towns where they can be found.

	* dks/model/preferences.py (isTagged): Changed the tagged list to
	a dictionary because it seemed better to check for a hash key of
	the name instead of searching for a string in a list (not sure if
	this is true or not).  Also avoids the need to check for
	duplicates.

	* dks/view/shop.py (Shop.initPotions): Changed the way the filter
	works, so just in case the bug comes back where potions are tagged
	multiple times it shouldn't affect they way the filter works.

2005-11-10  Jakim Friant  <jfriant@glec.cfcc.edu>

	* dks/controller.py (StandardScreen): Created a new class in the
	new controller module to serve as the parent class for all the
	screens in the view package.  That way I can centralize the main()
	and run() functions as well as provide default commands that work
	in every screen without duplicating code.  Also the StandardDialog
	class has been moved here and inherits from the StandardScreen
	class, while changing some of the default command keys'

	* dks/view/craft.py (Craft.__init__): Improved the headers on the
	two dialogs: craft and shopping_list.

	* dks/view/city.py (City.allQuests): Changed the dialog that
	displays the quests to a simple menu dialog.
	(City.__init__): Added a check box to flag quests as completed.
	(City.save,City.updateScreen): Quests are now updated as being
	done if flagged, which prevents them being displayed on the quest
	report, but they still need to be deleted manually (usually by
	overwriting).

2005-11-08  Jakim Friant  <jfriant@glec.cfcc.edu>

	* dks/view/shopping_list.py
	(ShoppingList.__init__.ingredientColumn): Fixed the formatting of
	the columns.
	(ShoppingList.__init__): Moved the code to load the cart data from
	a pickle here and left the option to pass a car object in.  Also
	had to change the way the data members were created so that they
	would end up in the __dict__ and get pickled along with the rest
	of the class.

	* dks/view/std_dlg.py (StandardDialog.palette): Created a parent
	class to use for all custom dialogs, for now just Craft and
	ShoppingList.

	* dks/view/shop.py (Shop.shop): Moved the shopping dialog into its
	own module to be able to display a custom dialog.

	* Makefile (dist): Added a perl one-line script to increment the
	build number in the version by one everytime I build a new dist
	file.

2005-11-07  Jakim Friant  <jfriant@glec.cfcc.edu>

	* dks/const.py (standard_palette): Moved the urwid palette here to
	have it the same across screens.
	(dlg_h,dlg_w): Set max dialog width and height as constants for
	use with all the dialog functions.

	* dks/view/city.py (cCity.shoppingList): Added the last shopping
	list to the city view too.
	(cCity.help): ...and added a help screen.

	* dks/view/shop.py (cShop.shop): Added the towns where the
	ingredients can be found to the shopping list.
	(cShop.show): Added a function to redisplay the last shopping list.

2005-10-21  Jakim Friant  <jfriant@glec.cfcc.edu>

	* dks/const.py (shopping_file): Getting ready to have a number of
	paths that the program will check for a shopping list output file.

	* potion.py (cFormula.__init__): I'm no longer storing the tagged
	value in the potions file so they all default to False now.
