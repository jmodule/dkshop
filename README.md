# NAME

DKShop - Alchemy component shopping list program

# SUMMARY

python dkshop.py

# DESCRIPTION

I wanted a way to quickly calcluate how many of each ingredient I
needed to make a batch of potions.  So I came up with the idea of
creating a virtual shopping list where I specify how may potions from
each formula I want to make and then the program tells me how many of
each ingredient I'll need to buy.  That way I won't get confused and
forget to buy enough Sanguine Base, or some such.

# AUTHOR

Jakim Friant <jakim@friant.org>
