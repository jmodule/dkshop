.PHONY: all dist clean proper test

all: dist

dist:
	perl -ne 'if (/_build = (\d+)/) { $$new = $$1 + 1; s/\d+/$$new/; }; print $$_' dks/const.py > dks/const.new && mv dks/const.new dks/const.py
	python setup.py sdist

clean:
	rm -f README
	rm -f MANIFEST
	rm -f last_list.*
	rm -rf build
	rmb

proper: clean
	rm -rf dist
	-rm test/*.db
	-rm test/*.csv
	-rm test/*.txt
	-rm test/*.pkl
	-rm -r dkshop.egg-info

test:
	cp *.csv test
	cp waterways.txt test
	python test/test_db.py
	python test/test_prefs.py
	python init_db.py

install:
	sed 's/mode = .test./mode = "live"/' dks/const.py > dks/const.tmp && \
	mv dks/const.tmp dks/const.py
	python setup.py install --prefix $$HOME
